﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TesteTecnico.ApiVenda.Domain.Entities;

namespace TesteTecnico.ApiVenda.Data.Context
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> option)
            : base(option) { }

        public DbSet<Produto> Produto { get; set; }
        public DbSet<Vendedor> Vendedor { get; set; }
        public DbSet<Venda> Venda { get; set; }
        public DbSet<StatusVenda> StatusVenda { get; set; }

        public int SaveChanges()
        {
            foreach (var entry in ChangeTracker.Entries().Where(entry => entry.Entity.GetType().GetProperty("dataVenda") != null))
            {
                if (entry.State == EntityState.Added)
                {
                    entry.Property("dataVenda").CurrentValue = DateTime.Now;
                    entry.Property("status").CurrentValue = "Aguardando pagamento";
                }
                if (entry.State == EntityState.Modified)
                {
                    entry.Property("dataVenda").IsModified = false;
                }

            }
            return base.SaveChanges();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Venda>()
            .Property(f => f.id)
            .ValueGeneratedOnAdd();

            modelBuilder.Entity<StatusVenda>(
                x =>
                {
                    x.HasNoKey();
                });

            base.OnModelCreating(modelBuilder);
        }
    }
}
