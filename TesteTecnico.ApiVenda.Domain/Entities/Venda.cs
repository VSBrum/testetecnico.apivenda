﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TesteTecnico.ApiVenda.Domain.Entities
{
    public class Venda
    {
        public int id { get; set; }

        public DateTime dataVenda { get; set; }

        public List<Produto> Produtos { get; set; }

        public Vendedor Vendedor { get; set; }

        public string status { get; set; }
    }
}
